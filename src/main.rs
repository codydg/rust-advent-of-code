use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    println!("Filename: {}", filename);
    let lines = contents.trim_end().split("\n").map(|x| x.trim_end()).collect::<Vec<_>>();
    for line in lines {
        println!("{}", line);
    }
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}
